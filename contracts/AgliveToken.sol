// SPDX-License-Identifier: Aglive
pragma solidity ^0.6.2;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract AgliveToken is ERC721, AccessControl {

    using Counters for Counters.Counter;
    using EnumerableSet for EnumerableSet.UintSet;

    Counters.Counter private _tokenId;

    bytes32 public constant MODERATOR_ROLE = keccak256("MODERATOR");
    bytes32 public constant CUSTODIAN_ROLE = keccak256("CUSTODIAN");
    bytes32 public constant USER_ROLE = keccak256("USER");

    constructor(address root) public ERC721("Aglive", "AGL") {
        _setupRole(DEFAULT_ADMIN_ROLE, root);
        _setRoleAdmin(MODERATOR_ROLE, DEFAULT_ADMIN_ROLE);
        _setRoleAdmin(CUSTODIAN_ROLE, DEFAULT_ADMIN_ROLE);
        _setRoleAdmin(USER_ROLE, DEFAULT_ADMIN_ROLE);
    }

    struct Token {
        uint256 tokenId;
        address creator;
        string tokenType;
        string tokenTag;
        uint256 timestamp;
    }

    struct Activity {
        string activityType;
        string activityTag;
        uint256 timestamp;
    }

    event TokenCreated(address indexed to, uint256 indexed tokenId);
    event ActivityRecorded(address indexed sender, uint256 indexed tokenId, string activityType, string activityTag);
    event TokenTransfer(address indexed from, address indexed to, uint256 indexed tokenId);

    mapping(address => EnumerableSet.UintSet) private _holderSubjectTokens;
    mapping(uint256 => Activity[]) internal activities;
    mapping(uint256 => mapping(string => Activity[])) internal activitiesWithType;
    mapping(uint256 => mapping(string => string[])) internal activityWithTypeTag;
    mapping(address => Token) internal tokenOwner;
    mapping(uint256 => Token) internal tokenWithTokenId;

    modifier onlyAdmin() {
        require(isAdmin(msg.sender), "Restricted to admins.");
        _;
    }

    function isAdmin(address account) public virtual view returns (bool) {
        return hasRole(DEFAULT_ADMIN_ROLE, account);
    }

    function isModerator(address account) public virtual view returns (bool) {
        return hasRole(MODERATOR_ROLE, account);
    }

    function isCustodian(address account) public virtual view returns (bool) {
        return hasRole(CUSTODIAN_ROLE, account);
    }

    function isUser(address account) public virtual view returns (bool) {
        return hasRole(USER_ROLE, account);
    }

    function addAdmin(address account) public virtual onlyAdmin {
        grantRole(DEFAULT_ADMIN_ROLE, account);
    }

    function addModerator(address account) public virtual onlyAdmin {
        grantRole(MODERATOR_ROLE, account);
    }

    function addUser(address account) public virtual onlyAdmin {
        grantRole(USER_ROLE, account);
    }

    function addCustodian(address account) public virtual onlyAdmin {
        grantRole(CUSTODIAN_ROLE, account);
    }
    
    function createToken (string calldata tokenType, string calldata tokenTag) external {
        require(validateRole(msg.sender, tokenType, tokenTag), "Restricted to Moderators and Custodians with a valid tag.");

        uint256 tokenId = _generateTokenId();

        Token memory token = Token(
            tokenId,
            msg.sender,
            tokenType,
            tokenTag,
            now
        );

        tokenWithTokenId[tokenId] = token;

        if (keccak256(abi.encodePacked(tokenType)) == keccak256(abi.encodePacked("subject"))) {
            _holderSubjectTokens[msg.sender].add(tokenId);
        }

        super._mint(msg.sender, tokenId);

        emit TokenCreated(msg.sender, tokenId);
    }

    function validateRole (address account, string memory vType, string memory vTag) internal view returns (bool) {
        bool check = false;

        if (isModerator(account)) {
            if (keccak256(abi.encodePacked(vType)) == keccak256(abi.encodePacked("subject"))) {
                check = true;
            }
        }

        else if (isCustodian(account)) {
            if (keccak256(abi.encodePacked(vType)) == keccak256(abi.encodePacked("object")) || keccak256(abi.encodePacked(vType)) == keccak256(abi.encodePacked("activity"))) {
                string[] memory accountTags = _getTags(account);
                for(uint i = 0; i < accountTags.length; i++) {
                    if (keccak256(abi.encodePacked(vTag)) == keccak256(abi.encodePacked(accountTags[i]))) {
                        check = true;
                    }
                }
            }
        }

        return check;
    }

    function _generateTokenId() internal returns (uint256) {
        _tokenId.increment();
        return _tokenId.current();
    }

    function getTokenById(uint256 tokenId) external view returns (Token memory, Activity[] memory) {
        if (isModerator(msg.sender) || isCustodian(msg.sender) || isUser(msg.sender)) {
            require(super._exists(tokenId), "Token does not exist");
            require(_checkTokenTag(msg.sender, tokenId), "Restricted to users with valid tags.");

            return (
                tokenWithTokenId[tokenId],
                activities[tokenId]
            );
        }
        else {
            require(isUser(msg.sender), "Restricted to users.");
        }
    }

    function _checkTokenTag(address account, uint256 tokenId) internal view returns (bool) {
        string[] memory accountTags = _getTags(account);
        string memory tokenTag = tokenWithTokenId[tokenId].tokenTag;
        bool check = false;

        for(uint i = 0; i < accountTags.length; i++) {
            if (keccak256(abi.encodePacked(tokenTag)) == keccak256(abi.encodePacked(accountTags[i]))) {
                check = true;
            }
        }

        return (check);
    }

    function _getTags(address account) internal view returns (string[] memory){
        uint256 totalToken = balanceOf(account);
        uint256 tokenCount = _holderSubjectTokens[account].length();
        string[] memory tokenTags = new string[](tokenCount);
        uint256 tokenId;

        for(uint i = 0; i < totalToken; i++) {
            tokenId = (super.tokenOfOwnerByIndex(account, i));
            
            if (keccak256(abi.encodePacked(tokenWithTokenId[tokenId].tokenType)) == keccak256(abi.encodePacked("subject"))) {
                tokenCount--;
                string storage tokenTag = tokenWithTokenId[tokenId].tokenTag;
                tokenTags[tokenCount] = tokenTag;
            }
        }

        return tokenTags;
    }
    
    function addActivity(uint256 tokenId, string calldata activityType, string calldata activityTag) external {
        string memory vType = "activity";
        require (validateRole(msg.sender, vType, activityTag), "Restricted to Custodians with a valid tag.");

        activities[tokenId].push(
            Activity(activityType, activityTag, now)
        );

        activitiesWithType[tokenId][activityType].push(
            Activity(activityType, activityTag, now)
        );

        activityWithTypeTag[tokenId][activityType].push(activityTag);

        emit ActivityRecorded(msg.sender, tokenId, activityType, activityTag);
    }

    function getActivityByType(uint256 tokenId, string calldata activityType) external view returns (Activity[] memory) {
        if (isModerator(msg.sender) || isCustodian(msg.sender) || isUser(msg.sender)) {
            require(super._exists(tokenId), "Token does not exist");
            require(_checkActivityTag(msg.sender, tokenId, activityType), "Restricted to users with a valid tag.");

            return activitiesWithType[tokenId][activityType];
        }
        else {
            require(isUser(msg.sender), "Restricted to users.");
        }
    }

    function _checkActivityTag(address account, uint256 tokenId, string memory activityType) internal view returns (bool) {
        string[] memory accountTags = _getTags(account);
        string[] memory activityTag = activityWithTypeTag[tokenId][activityType];
        bool check = false;

        for(uint i = 0; i < accountTags.length; i++) {
            for(uint j = 0; j < activityTag.length; j++) {
                if (keccak256(abi.encodePacked(activityTag[j])) == keccak256(abi.encodePacked(accountTags[i]))) {
                check = true;
                }
            }
        }

        return (check);
    } 

    function transferToken (address from, address to, uint256 tokenId) external {
        require(to != address(0x0), "Receiver cannot be an empty address!");
        require(super._isApprovedOrOwner(from, tokenId));

        super.safeTransferFrom(from, to, tokenId, "");
        _holderSubjectTokens[from].remove(tokenId);
        _holderSubjectTokens[to].add(tokenId);

        emit TokenTransfer(from, to, tokenId);
    }
}