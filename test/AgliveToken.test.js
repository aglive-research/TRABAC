const AgliveToken = artifacts.require("./AgliveToken.sol");
const truffleAssert = require("truffle-assertions");

contract("AgliveToken", accounts => {

    let agliveToken

    // Addresses and roles
    const DEFAULT_ADMIN_ROLE = accounts[0]
    const MODERATOR_ROLE = accounts[1]
    const CUSTODIAN_ROLE_C = accounts[2]
    const CUSTODIAN_ROLE_D = accounts[3]
    const CUSTODIAN_ROLE_E = accounts[4]
    const USER_ROLE_F = accounts[5]
    const USER_ROLE_G = accounts[6]
    const USER_ROLE_H = accounts[7]
    const USER_ROLE_I = accounts[8]
    const USER_ROLE_J = accounts[9]

    before(async () => {
        agliveToken = await AgliveToken.deployed()
    })

    describe('DELEGATION OF ROLE BY DEFAULT_ADMIN_ROLE', async () => {

        it('Verify Address A status as DEFAULT_ADMIN_ROLE', async () => {
            const result = await agliveToken.isAdmin(DEFAULT_ADMIN_ROLE)
            assert.ok(result)
        })

        it('Address A add Address B as MODERATOR_ROLE', async () => {
            const result = await agliveToken.addModerator(MODERATOR_ROLE)
            const event = result.logs[0].args
            assert.equal(event.account, MODERATOR_ROLE)
        })

        it('Address A add Address C as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.addCustodian(CUSTODIAN_ROLE_C)
            const event = result.logs[0].args
            assert.equal(event.account, CUSTODIAN_ROLE_C)
        })

        it('AAddress A adddd Address D as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.addCustodian(CUSTODIAN_ROLE_D)
            const event = result.logs[0].args
            assert.equal(event.account, CUSTODIAN_ROLE_D)
        })

        it('Address A add Address E as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.addCustodian(CUSTODIAN_ROLE_E)
            const event = result.logs[0].args
            assert.equal(event.account, CUSTODIAN_ROLE_E)
        })

        it('Address A add Address F as USER_ROLE', async () => {
            const result = await agliveToken.addUser(USER_ROLE_F)
            const event = result.logs[0].args
            assert.equal(event.account, USER_ROLE_F)
        })

        it('Address A add Address G as USER_ROLE', async () => {
            const result = await agliveToken.addUser(USER_ROLE_G)
            const event = result.logs[0].args
            assert.equal(event.account, USER_ROLE_G)
        })

        it('Address A add Address H as USER_ROLE', async () => {
            const result = await agliveToken.addUser(USER_ROLE_H)
            const event = result.logs[0].args
            assert.equal(event.account, USER_ROLE_H)
        })

        it('Address A add Address I as USER_ROLE', async () => {
            const result = await agliveToken.addUser(USER_ROLE_I)
            const event = result.logs[0].args
            assert.equal(event.account, USER_ROLE_I)
        })

        it('Address A add Address J as USER_ROLE', async () => {
            const result = await agliveToken.addUser(USER_ROLE_J)
            const event = result.logs[0].args
            assert.equal(event.account, USER_ROLE_J)
        })

    })

    describe('TOKEN CREATION AND TOKEN TRANSFER BY MODERATOR_ROLE', async() => {

        it('Verify Address B status as MODERATOR_ROLE', async () => {
            const result = await agliveToken.isModerator(MODERATOR_ROLE, {'from': MODERATOR_ROLE})
            assert.ok(result)
        })

        it('Address B create 7 token subjects', async () => {
            const token1 = await agliveToken.createToken("subject", "supplier", {'from': MODERATOR_ROLE})
            assert.equal(token1.logs[1].args.tokenId.toNumber(), 1)
            const token2 = await agliveToken.createToken("subject", "transport", {'from': MODERATOR_ROLE})
            assert.equal(token2.logs[1].args.tokenId.toNumber(), 2)
            const token3 = await agliveToken.createToken("subject", "inspection", {'from': MODERATOR_ROLE})
            assert.equal(token3.logs[1].args.tokenId.toNumber(), 3)
            const token4 = await agliveToken.createToken("subject", "supplier", {'from': MODERATOR_ROLE})
            assert.equal(token4.logs[1].args.tokenId.toNumber(), 4)
            const token5 = await agliveToken.createToken("subject", "transport", {'from': MODERATOR_ROLE})
            assert.equal(token5.logs[1].args.tokenId.toNumber(), 5)
            const token6 = await agliveToken.createToken("subject", "inspection", {'from': MODERATOR_ROLE})
            assert.equal(token6.logs[1].args.tokenId.toNumber(), 6)
            const token7 = await agliveToken.createToken("subject", "warehouse", {'from': MODERATOR_ROLE})
            assert.equal(token7.logs[1].args.tokenId.toNumber(), 7)
        })

        it('Address B transfer token subject (supplier) to Address C', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, CUSTODIAN_ROLE_C, 1, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, CUSTODIAN_ROLE_C)
        })

        it('Address B transfer token subject (transport) to Address D', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, CUSTODIAN_ROLE_D, 2, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, CUSTODIAN_ROLE_D)
        })

        it('Address B transfer token subject (inspection) to Address E', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, CUSTODIAN_ROLE_E, 3, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, CUSTODIAN_ROLE_E)
        })

        it('Address B transfer token subject (supplier) to Address F', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, USER_ROLE_F, 4, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, USER_ROLE_F)
        })

        it('Address B transfer token subject (transport) to Address G', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, USER_ROLE_G, 5, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, USER_ROLE_G)
        })

        it('Address B transfer token subject (inspection) to Address H', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, USER_ROLE_H, 6, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, USER_ROLE_H)
        })

        it('Address B transfer token subject (warehouse) to Address I', async () => {
            const result = await agliveToken.transferToken(MODERATOR_ROLE, USER_ROLE_I, 7, {'from': MODERATOR_ROLE})
            assert.equal(result.logs[2].args.to, USER_ROLE_I)

        })

    })

    describe('TOKEN AND ACTIVITY CREATION BY CUSTODIAN_ROLE', async () => {

        it('Verify Address C status as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.isCustodian(CUSTODIAN_ROLE_C, {'from': CUSTODIAN_ROLE_C})
            assert.ok(result)
        })

        it('Verify Address D status as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.isCustodian(CUSTODIAN_ROLE_D, {'from': CUSTODIAN_ROLE_D})
            assert.ok(result)
        })

        it('Verify Address E status as CUSTODIAN_ROLE', async () => {
            const result = await agliveToken.isCustodian(CUSTODIAN_ROLE_E, {'from': CUSTODIAN_ROLE_E})
            assert.ok(result)
        })

        it('Address C create token object (supplier, Token8)', async () => {
            const result = await agliveToken.createToken("object", "supplier", {'from': CUSTODIAN_ROLE_C})
            assert.equal(result.logs[1].args.tokenId.toNumber(), 8)
        })

        it('Address D create token object (transport, Token9)', async () => {
            const result = await agliveToken.createToken("object", "transport", {'from': CUSTODIAN_ROLE_D})
            assert.equal(result.logs[1].args.tokenId.toNumber(), 9)
        })

        it('Address C create activity (data_induction, supplier) for Token8', async () => {
            const result = await agliveToken.addActivity(8, "data_induction", "supplier", {'from': CUSTODIAN_ROLE_C})
            assert.equal(result.logs[0].args.tokenId.toNumber(), 8)
            assert.equal(result.logs[0].args.activityTag, "supplier")
        })

        it('Address D create activity (transfer, transport) for Token8', async () => {
            const result = await agliveToken.addActivity(8, "transfer", "transport", {'from': CUSTODIAN_ROLE_D})
            assert.equal(result.logs[0].args.tokenId.toNumber(), 8)
            assert.equal(result.logs[0].args.activityTag, "transport")
        })

        it('Address D create activity (travel_doc, transport) for Token9', async () => {
            const result = await agliveToken.addActivity(9, "travel_doc", "transport", {'from': CUSTODIAN_ROLE_D})
            assert.equal(result.logs[0].args.tokenId.toNumber(), 9)
            assert.equal(result.logs[0].args.activityTag, "transport")
        })

        it('Address E create activity (custom_doc, inspection) for Token9', async () => {
            const result = await agliveToken.addActivity(9, "custom_doc", "inspection", {'from': CUSTODIAN_ROLE_E})
            assert.equal(result.logs[0].args.tokenId.toNumber(), 9)
            assert.equal(result.logs[0].args.activityTag, "inspection")
        })

    })

    describe('ACCESS REQUEST BY ADDRESS C (CUSTODIAN_ROLE, SUPPLIER)', async () => {

        it('Address C access request for Token8 (supplier)', async () => {
            const result = await agliveToken.getTokenById(8, {'from': CUSTODIAN_ROLE_C})
            assert.ok(result)
        })

        it('[Failure of access] Address C access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': CUSTODIAN_ROLE_C}))
        })

        it('Address C access request for activity (data_induction, supplier) for Token8', async () => {
            const result = await agliveToken.getActivityByType(8, "data_induction", {'from': CUSTODIAN_ROLE_C})
            assert.ok(result)
        })

        it('[Failure of access] Address C access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': CUSTODIAN_ROLE_C}))
        })

        it('[Failure of access] Address C access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': CUSTODIAN_ROLE_C}))
        })

        it('[Failure of access] Address C access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': CUSTODIAN_ROLE_C}))
        })

    })

    describe('ACCESS REQUEST BY ADDRESS D (CUSTODIAN_ROLE, TRANSPORT)', async () => {

        it('[Failure of access] Address D access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': CUSTODIAN_ROLE_D}))
        })

        it('Address D access request for Token9 (transport)', async () => {
            const result = await agliveToken.getTokenById(9, {'from': CUSTODIAN_ROLE_D})
            assert.ok(result)
        })

        it('[Failure of access] Address D access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': CUSTODIAN_ROLE_D}))
        })

        it('Address D access request for activity (transfer, transport) for Token8', async () => {
            const result = await agliveToken.getActivityByType(8, "transfer", {'from': CUSTODIAN_ROLE_D})
            assert.ok(result)
        })

        it('Address D access request for activity (travel_doc, transport) for Token8', async () => {
            const result = await agliveToken.getActivityByType(9, "travel_doc", {'from': CUSTODIAN_ROLE_D})
            assert.ok(result)
        })

        it('[Failure of access] Address D access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': CUSTODIAN_ROLE_D}))
        })

    })

    describe('ACCESS REQUEST BY ADDRESS E (CUSTODIAN_ROLE, INSPECTION)', async () => {

        it('[Failure of access] Address E access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': CUSTODIAN_ROLE_E}))
        })

        it('[Failure of access] Address E access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': CUSTODIAN_ROLE_E}))
        })

        it('[Failure of access] Address E access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': CUSTODIAN_ROLE_E}))
        })

        it('[Failure of access] Address E access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': CUSTODIAN_ROLE_E}))
        })

        it('[Failure of access] Address E access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': CUSTODIAN_ROLE_E}))
        })

        it('Address E access request for activity (custom_doc, inspection) for Token8', async () => {
            const result = await agliveToken.getActivityByType(9, "custom_doc", {'from': CUSTODIAN_ROLE_E})
            assert.ok(result)
        })

    })

    describe('ACCESS REQUEST BY ADDRESS F (USER_ROLE, SUPPLIER)', async () => {

        it('Address F access request for Token8 (supplier)', async () => {
            const result = await agliveToken.getTokenById(8, {'from': USER_ROLE_F})
            assert.ok(result)
        })

        it('[Failure of access] Address F access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': USER_ROLE_F}))
        })

        it('Address F access request for activity (data_induction, supplier) for Token8', async () => {
            const result = await agliveToken.getActivityByType(8, "data_induction", {'from': USER_ROLE_F})
            assert.ok(result)
        })

        it('[Failure of access] Address F access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': USER_ROLE_F}))
        })

        it('[Failure of access] Address F access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': USER_ROLE_F}))
        })

        it('[Failure of access] Address F access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': USER_ROLE_F}))
        })

    })

    describe('ACCESS REQUEST BY ADDRESS G (USER_ROLE, TRANSPORT)', async () => {

        it('[Failure of access] Address G access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': USER_ROLE_G}))
        })

        it('Address G access request for Token9 (transport)', async () => {
            const result = await agliveToken.getTokenById(9, {'from': USER_ROLE_G})
            assert.ok(result)
        })

        it('[Failure of access] Address G access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': USER_ROLE_G}))
        })

        it('Address G access request for activity (transfer, transport) for Token8', async () => {
            const result = await agliveToken.getActivityByType(8, "transfer", {'from': USER_ROLE_G})
            assert.ok(result)
        })

        it('Address G access request for activity (travel_doc, transport) for Token8', async () => {
            const result = await agliveToken.getActivityByType(9, "travel_doc", {'from': USER_ROLE_G})
            assert.ok(result)
        })

        it('[Failure of access] Address G access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': USER_ROLE_G}))
        })

    })

    describe('ACCESS REQUEST BY ADDRESS H (USER_ROLE, INSPECTION)', async () => {

        it('[Failure of access] Address H access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': USER_ROLE_H}))
        })

        it('[Failure of access] Address H access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': USER_ROLE_H}))
        })

        it('[Failure of access] Address H access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': USER_ROLE_H}))
        })

        it('[Failure of access] Address H access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': USER_ROLE_H}))
        })

        it('[Failure of access] Address H access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': USER_ROLE_H}))
        })

        it('Address H access request for activity (custom_doc, inspection) for Token8', async () => {
            const result = await agliveToken.getActivityByType(9, "custom_doc", {'from': USER_ROLE_H})
            assert.ok(result)
        })

    })

    describe('ACCESS REQUEST BY ADDRESS I (USER_ROLE, WAREHOUSE)', async () => {

        it('[Failure of access] Address I access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': USER_ROLE_I}))
        })

        it('[Failure of access] Address I access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': USER_ROLE_I}))
        })

        it('[Failure of access] Address I access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': USER_ROLE_I}))
        })

        it('[Failure of access] Address I access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': USER_ROLE_I}))
        })

        it('[Failure of access] Address I access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': USER_ROLE_I}))
        })

        it('[Failure of access] Address I access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': USER_ROLE_I}))
        })

    })

    describe('ACCESS REQUEST BY ADDRESS J (USER_ROLE, NO TOKEN SUBJECT)', async () => {

        it('[Failure of access] Address J access request for Token8 (supplier)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(8, {'from': USER_ROLE_J}))
        })

        it('[Failure of access] Address J access request for Token9 (transport)', async () => {
            await truffleAssert.fails(agliveToken.getTokenById(9, {'from': USER_ROLE_J}))
        })

        it('[Failure of access] Address J access request for activity (data_induction, supplier) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "data_induction", {'from': USER_ROLE_J}))
        })

        it('[Failure of access] Address J access request for activity (transfer, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(8, "transfer", {'from': USER_ROLE_J}))
        })

        it('[Failure of access] Address J access request for activity (travel_doc, transport) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "travel_doc", {'from': USER_ROLE_J}))
        })

        it('[Failure of access] Address J access request for activity (custom_doc, inspection) for Token8', async () => {
            await truffleAssert.fails(agliveToken.getActivityByType(9, "custom_doc", {'from': USER_ROLE_J}))
        })

    })

})