# TRABAC: A Non-Fungible Token Role-Attribute Based Access Control using Smart Contract for The Supply Chain

[[_TOC_]]

## What is TRABAC?

TRABAC is a composite two-level access control that utilised ERC-721 NFT token by combining the simplicity of RBAC (Role-Based Access Control) and the flexibility of ABAC (Attribute-Based Access Control).

```mermaid
graph LR
    A[Start] --> B{Level 1:<br>RBAC}
    B -->|Yes| C{Level 2:<br>ABAC}
    C -->|Yes| E[/Access<br>Granted/]
    E --> F[End]
    B -->|No| D[/Access<br>Denied/]
    C -->|No| D

    style B fill:#2f8,stroke:#333,stroke-width:2px
    style C fill:#2f8,stroke:#333,stroke-width:2px
```

### Level 1: RBAC (Role-Based Access Control)
 
Proposed RBAC roles in TRABAC:

| Role | Descriptions |
| ---  | ---          |
| `Admin` | The `Admin` role is assigned to the contrat owner by default. It can grant ~~and revoke~~ [^1] user permissions to any account. |
| `Moderator` | The `Moderator` role can create and read token type subject. |
| `Custodian` | The `Custodian` role can create and read token type object. The `Custodian` role can also create and read attached activities for token object. |
| `User` | The `User` role can read information from token type object. |

It should be noted that these roles are distinct without overlap in their access abilities. In particular, while the `Admin` can assign any address to any role, the `Admin` itself does not have the ability to act in those roles (e.g. create and read token). This restriction is intentional as the less we use an account or address, the less likely it is that we somehow compromise the account. 

[^1]: The revoke function is excluded from the contract implementation because it might exceed contract size limit.

### Level 2: ABAC (Attribute-Based Access Control)

The basic AgliveToken (`AGL`) token structure can be descibes as `AGL`<sub>`ID`</sub> = {`tType`, `tTag`, `tMeta`} where the parameters for `AGL` are describes as:
- `ID`, the identifier for associated token
- `A`, the address of token holder or owner
- `tType`, token type = { `subject`, `object` }
- `tTag`, token attribute tag assigned to the token
- `tMeta`, metadata attached to the token (e.g. JSON file, image or link) [^2]

To track cganges to the token that had been added to the blockchain, we introduces a child to the token called `Activity` or `AC`. The `Activity` (`AC`) is exclusively for token type object only. `Activity` (`AC`) is describes as `AC`[`AGL`<sub>ID</sub>] = {`aType`, `tTag`, `tMeta`} where the parameters are:
- `AGL`<sub>`ID`</sub>, the `AGL` token `ID` attached to the `Activity`
- `aType`, activity type
- `aTag`, attribute tag assigned to the `Activity`
- `aMeta`, metadata attached to the token (e.g. JSON file, image or link) [^2]

[^2]: Current `AgliveToken` contract excluded `tMeta` and `aMeta` from its implementation because it might exceed contract size limit. 

The `tTag` and `aTag` may indicate:
- name of the organisation in the supply chain
- the phase of the supply chain (e.g. suplier, transport, warehouse, inspection, etc.)

## Prerequisite 

- [NodeJS](https://nodejs.org/en/)
- Ganache CLI v.6.12.1
- Truffle

## Experiment Setup

1. Create folder for node project (e.g. `TRABAC`)
2. Initialise node: 
```shell
npm init -y
```
3. Install Ganache (to setup local blockchain): 
```shell
npm install --save-dev ganache-cli
```
4. Install Truffle: 
```shell
npm install --save-dev truffle
```
5. Initialise Truffle: 
```shell
npx truffle init
```
6. Import OpenZeppelin contracts: 
```shell
npm install --save-dev @openzeppelin/contracts@3.2.0
```
7. Update Solidity compiler version in "truffle-config.js":
```javascript
compiler : {
    solc : {
        version : "0.6.2",
    }
}
```
8. Add `Development` network to localhost and port 8545 in "truffle-config.js":
```javascript
networks : {
    development : {
        host : "127.0.0.1",
        port : 8545,
        network_id : "*",
    },
}
```
9. Import TRABAC contract to folder contracts: 
```shell
curl -o ./contracts/AgliveToken.sol -k https://gitlab.com/aglive-research/TRABAC/-/raw/master/contracts/AgliveToken.sol
```
10. Run Ganache-CLI in deterministic mode (in a new tab): 
> This step is to setup local blockchain in deterministic mode so we get the same address every time. Do note, all transactions on the blockchain is done on local Ganache-CLI. Thus, if you close the terminal, all transactions will be lost. So, leave this tab open throughout your experiment. 
```shell
npx ganache-cli --deterministic
```
11. Copy the first Ganache generated account address (e.g. `0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1`)
12. Create migration file (migrations/2_deploy.js):
```javascript
const AgliveToken = artifacts.require("AgliveToken");

module.exports = function (deployer) {
    // Replace 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1 with previously copied address in Step 11.
    deployer.deploy(AgliveToken, "0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1");
};
```
13. Compile contract: 
```shell
npx truffle compile
```
14. Deploy `AgliveToken` contract to `Development` network: 
```shell
npx truffle migrate --network development
```

## Run Experiment

1. Run Ganache in deterministic mode (preferably in a new tab, if its not already running): 
```shell
npx ganache-cli --deterministic
```
2. Install truffle-assertions
```shell
npm install truffle-assertions
```
3. Import TRABAC test scripts to folder test: 
```shell
curl -o ./test/AgliveToken.test.js -k https://gitlab.com/aglive-research/TRABAC/-/raw/master/test/AgliveToken.test.js
```
4. Run test 
```shell
npx truffle test
```

![Test simulation result](img/test_simulation.png)
